# ePayco
	Billetera vitual

# Descargar las dependencias
    cd epayco_app
	yarn install							[requerimientos react]
    cd api
    composer install                        [requerimientos lumen]
# Configurar el JWT con los siguientes comandos
    php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
    php artisan jwt:secret

# Agregar base de datos al .env ubicado en la carpeta /api como .env.example (`.env`)
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=(BASE DE DATOS)
    DB_USERNAME=(USUARIO DE LA BD)
    DB_PASSWORD=(CONTRASEÑA)

# Correr las migraciones 
    php artisan migrate

# Iniciar el servidor php para la api lumen
	php -S localhost:8000 -t public  			[podras ver el servidor corriendo en la siguiente ruta `localhost:8000`]

# Iniciar el servidor de desarrollo de react
	yarn start 								[podras ver el entorno de desarrollo de react corriendo en el navegador con la siguiente ruta `localhost:3000`]