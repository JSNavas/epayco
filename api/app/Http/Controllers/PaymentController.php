<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\User;
use App\Models\Wallet;
use Webpatser\Uuid\Uuid;
use App\Http\Services\UtilityService;
use Illuminate\Support\Str;
use App\Mail\PaymentConfirmation;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $utilityService;

    public function __construct()
    {
        $this->middleware('auth:user');
        $this->utilityService = new UtilityService;
    }

    public function index($token)
    {
        $payment = Payment::where('token', $token)->first();
        return response()->json($payment);
    }

    public function show(Request $request)
    {
        $id_session = Str::uuid($request->id_session);
        $payment = Payment::where('token',$request->token)->first();

        if($payment) {
            $responseMessage = "El token ha sido validado";
            return $this->utilityService->is200ResponseWithData($responseMessage, $payment);
        }else {
            $responseMessage = "El token es inválido";
            return $this->utilityService->is422Response($responseMessage);
        }

    }

    public function store(Request $request)
    {
        $wallet = Wallet::where('user_id', $request->user_id)->first();
        $user = User::find($request->user_id);
        if($wallet->money > $request->amount){
            $payment = new Payment();

            $payment->amount = $request->amount;
            $payment->token = rand(0, 999999);
            $payment->session_id = Uuid::generate()->string;
            $payment->user_id = $request->user_id;
            $payment->save();

            $email = $user->email;
            $mailInfo = [
                'description' => 'Confirme su pago haciendo clic en el enlace a continuación y copie y pegue el siguiente token para validar su compra.',
                'url' => 'http://localhost:3000/confirm/session_id/'.$payment->session_id,
                'token' => $payment->token,
            ];

            Mail::to($email)->send(new PaymentConfirmation($mailInfo));

            $responseMessage = "Le hemos enviado un token a su correo, Ingréselo para confirmar su pago!!";
            return $this->utilityService->is200Response($responseMessage);



            return response()->json($wallet, 201);
        } else {
            return response()->json([
                "success" => false,
                "message" => 'No cuenta con saldo suficiente!',
            ],422);
        }

        // return response()->json($payment, 201);
    }
}
