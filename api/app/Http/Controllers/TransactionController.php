<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Transaction;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    public function store(Request $request)
    {
        // operation, amount, wallet_id, document_ID, mobile_phone
        $operation = $request->operation;

        switch ($operation) {
            case 'DEPOSIT':
                $wallet = Wallet::find($request->wallet_id);
                $user = User::find($wallet->user_id);
                if($request->document_ID == $user->document_ID && $request->mobile_phone == $user->mobile_phone){
                    $wallet->money = $wallet->money + $request->amount;
                    $wallet->save();

                    $transaction = new Transaction();

                    $transaction->operation_token = rand(0, 999999);
                    $transaction->amount = $request->amount;
                    $transaction->wallet_id = $request->wallet_id;
                    $transaction->operation = $request->operation;
                    $transaction->save();

                    return response()->json($transaction, 201);
                } else {
                    return response()->json([
                        "success" => false,
                        "message" => 'Los datos suministrados no coinciden!',
                    ],422);
                }
                break;
            case 'DEBIT':
                $wallet = Wallet::where('user_id', $request->user_id)->first();
                $user = User::find($request->user_id);
                if($wallet->money > $request->amount){
                    $wallet->money = $wallet->money - $request->amount;
                    $wallet->save();

                    $transaction = new Transaction();

                    $transaction->operation_token = rand(0, 999999);
                    $transaction->amount = $request->amount;
                    $transaction->wallet_id = $wallet->id;
                    $transaction->operation = $request->operation;
                    $transaction->save();

                    return response()->json($wallet, 201);
                } else {
                    return response()->json([
                        "success" => false,
                        "message" => 'No cuenta con saldo suficiente!',
                    ],422);
                }
                break;
            default:
                return response()->json('error', 401);
        }
    }
}
