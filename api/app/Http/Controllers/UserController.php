<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\UtilityService;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use App\Models\User;
use App\Models\Wallet;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $user;
    protected $wallet;
    protected $utilityService;

    public function __construct()
    {
        $this->middleware('auth:user', ['except' => ['login', 'register']]);
        $this->user = new User;
        $this->wallet = new Wallet;
        $this->utilityService = new UtilityService;
    }

    public function register(UserRegisterRequest $request)
    {
        $password_hash = $this->utilityService->hash_password($request->password);
        $new_user = $this->user->createUser($request, $password_hash);
        $this->wallet->createWallet($new_user->id);

        $success_message = 'Registrado exitosamente!!';
        return $this->utilityService->is200Response($success_message);
    }

    public function login(UserLoginRequest $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            $responseMessage = "Usuario o contraseña inválido";
            return $this->utilityService->is422Response($responseMessage);
        }
        return $this->respondWithToken($token);
    }

    public function home()
    {
        $responseMessage = "Datos de usuario cargados";
        $data = Auth::user();
        return $this->utilityService->is200ResponseWithData($responseMessage, $data);
    }

    public function logout()
    {
        try {
            Auth::logout();
            $responseMessage = "Se desconectó correctamente";
            return $this->utilityService->is200Response($responseMessage);

        } catch(TokenExpiredException $e){
            $responseMessage = "El token ya ha sido invalidado";
            return $this->utilityService->is422Response($responseMessage);
        }
    }

    public function refreshToken()
    {
        try {
            return $this->respondWithToken(Auth::refresh());

        } catch(TokenExpiredException $e){
            $responseMessage = "La actualización del token falló";
            return $this->utilityService->is422Response($responseMessage);
        }
    }

}
