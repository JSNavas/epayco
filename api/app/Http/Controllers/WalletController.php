<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wallet;

class WalletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    public function index()
    {
        $wallet = Wallet::all();
        return response()->json($wallet->load('transactions'));
    }

    public function show($id)
    {
        $wallet = Wallet::find($id);
        return response()->json($wallet->load('transactions'));
    }
}
