<?php

namespace App\Http\Requests;

use Urameshibr\Requests\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserLoginRequest extends FormRequest
{
    public function authorize()
    {
       return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ];
    }

    public function message()
    {
        return [
            'email.required' => 'El campo de correo electrónico es obligatorio',
            'email.email' => 'Por favor introduzca una dirección de correo electrónico válida',
            'password.required' => 'El campo de contraseña es obligatorio',
            'password.min' => 'Su contraseña debe tener 8 caracteres o más',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            "success" => false,
            "error" => $validator->errors(),
            "message" => 'Uno o más campos son obligatorios o no se ingresaron correctamente',
        ],422));
    }


}
