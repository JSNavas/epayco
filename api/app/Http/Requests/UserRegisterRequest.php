<?php

namespace App\Http\Requests;

use Urameshibr\Requests\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRegisterRequest extends FormRequest
{
    public function authorize()
    {
       return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
            'password_confirmation' => 'required ',
            'document_ID' => 'required|string',
            'mobile_phone' => 'required|string',
        ];
    }

    public function message()
    {
        return [
            'name.required' => 'El campo de nombres es obligatorio',
            'email.required' => 'El campo de correo electrónico es obligatorio',
            'email.email' => 'Por favor introduzca una dirección de correo electrónico válida',
            'email.unique' => 'Esta dirección de correo electrónico ya ha sido tomada',
            'password.required' => 'El campo de contraseña es obligatorio',
            'password_confirmation.required' => 'Confirme su contraseña',
            'password.min' => 'Su contraseña debe tener 8 caracteres o más',
            'document_ID.required' => 'El campo de documento de identidad es obligatorio.',
            'mobile_phone.required' => 'El campo del teléfono móvil es obligatorio',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            "success" => false,
            "error" => $validator->errors(),
            "message" => 'Uno o más campos son obligatorios o no se ingresaron correctamente',
        ],422));
    }


}
