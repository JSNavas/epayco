<?php

namespace Database\Factories;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    protected $model = Transaction::class;

    public function definition(): array
    {
    	return [
            'operation_token' => rand(0, 999999),
            'amount' => $this->faker->numberBetween(0, 1000),
            'wallet_id' => $this->faker->randomDigitNotNull,
    	];
    }
}
