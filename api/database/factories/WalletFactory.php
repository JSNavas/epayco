<?php

namespace Database\Factories;

use App\Models\Wallet;
use Illuminate\Database\Eloquent\Factories\Factory;

class WalletFactory extends Factory
{
    protected $model = Wallet::class;

    public function definition(): array
    {
    	return [
    	    'money' => $this->faker->numberBetween(0, 1000),
    	    'user_id' => 1,
    	];
    }
}
