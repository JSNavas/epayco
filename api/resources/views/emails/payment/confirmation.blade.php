@component('mail::message')
# Confirmación de pago.

{{ $mailInfo['description'] }}

# {{ $mailInfo['token'] }}

@component('mail::button', ['url' => $mailInfo['url']])
Confirmar Pago
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
