<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// API route group
$router->group(['prefix' => 'api/v1'], function () use ($router) {
    //api/v1/user/
    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->post('login', 'UserController@login');
        $router->post('register', 'UserController@register');
        $router->get('refresh-token', 'UserController@refreshToken');
        $router->get('/', 'UserController@home');
        $router->get('logout', 'UserController@logout');

        //api/v1/user/wallet
        $router->get('wallet', 'WalletController@index');
        $router->get('wallet/{id}', 'WalletController@show');

        //api/v1/user/payments
        $router->get('payments/{token}', 'PaymentController@index');
        $router->post('payments', 'PaymentController@store');
        $router->post('payments/confirm', 'PaymentController@show');

        //api/v1/user/transactions
        $router->post('transactions', 'TransactionController@store');

    });


});
