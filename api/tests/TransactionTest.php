<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Wallet;
use App\Models\Transaction;

class TransactionTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPostTransaction()
    {
        $wallet = Wallet::factory()->create();
        $transactions = Transaction::factory()->make();

        $response = $this->call('POST', 'api/v1/transactions', [
            'operation_token' => $transactions->operation_token,
            'amount' => $transactions->amount,
            'wallet_id' => $wallet->id
        ]);

        $response->assertJsonStructure([
            'id', 'operation_token', 'amount', 'wallet_id'
        ])->assertStatus(201);

        $this->seeInDatabase('transactions', [
            'operation_token' => $transactions->operation_token,
        ]);

        $this->seeInDatabase('wallets', [
            'id' => $wallet->id,
            'money' => $wallet->money + $transactions->amount,
        ]);
    }
}
