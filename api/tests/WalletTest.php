<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Wallet;
use App\Models\Transaction;

class WalletTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetWallet()
    {
        $wallet = Wallet::factory()->create();
        $transactions = Transaction::factory()->count(1)->create([
            'operation_token' => rand(0, 999999),
            'wallet_id' => $wallet->id
        ]);
        $response = $this->call('GET', 'api/v1/wallet');

        $this->assertEquals(200, $response->status());
        $response->assertStatus(200)
        ->assertJsonStructure([
            'id', 'money', 'transactions' => [
                '*' => [
                    'id', 'operation_token' , 'amount', 'wallet_id'
                ]
            ]
        ]);
        $this->assertCount(1, $response->json()['transactions']);
    }
}
