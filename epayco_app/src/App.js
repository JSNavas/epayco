import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
	Redirect,
} from "react-router-dom";
import { Provider as StoreProvider } from 'react-redux';
import { ToastProvider } from 'react-toast-notifications';
import MainNavbar from './components/MainNavbar';
import Wallet from './components/pages/Wallet';
import PaymentConfirm from './components/pages/PaymentConfirm';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import 'bootstrap/dist/css/bootstrap.min.css';
import store from './components/store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
    return (
		<ToastProvider
			autoDismiss
			autoDismissTimeout={6000}
		>
			<ToastContainer />
			<StoreProvider store={store}>
				<Router>
					<MainNavbar/>
					<Switch>
						<Route exact path="/">
							<Wallet />
						</Route>
						<Route path="/register">
							<Register />
						</Route>
						<Route path="/login">
							<Login />
						</Route>
						<Route path="/confirm/session_id/:session_id">
							<PaymentConfirm />
						</Route>
						<Route>
							<Redirect to="/" />
						</Route>
					</Switch>
				</Router>
			</StoreProvider>
		</ToastProvider>
    )
}

export default App;