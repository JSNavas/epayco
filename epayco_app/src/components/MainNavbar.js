import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logoutRequest } from './store/user';

const MainNavbar = () => {
    const dispatch = useDispatch();
    const userData = useSelector(state => state.user);

    const _logoutRequest = () => {
        dispatch(logoutRequest())
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light" style={{background: 'rgb(222 222 222)'}}>
            <div className="container">
                <Link className="navbar-brand" to="/">
                    <img src="images/epayco-logo.png" alt="epayco" width="100" height="34"/>
                </Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse d-flex flex-row-reverse" id="navbarNav">
                    <ul className="navbar-nav">
                        {
                            !!userData.loggedIn ?
                            (
                                <Fragment>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="#" onClick={_logoutRequest}>Cerrar sesion</Link>
                                    </li>
                                </Fragment>
                            )
                            
                            : (
                                <Fragment>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/register">Registrar</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/login">Iniciar</Link>
                                    </li>
                                </Fragment>
                            )
                        }
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default MainNavbar;
