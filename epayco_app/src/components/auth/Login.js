import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loginRequest, authValid } from '../store/user';
import { useForm } from 'react-hook-form';
import { Redirect } from 'react-router-dom';

const Login = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { loggedIn } = useSelector((state) => state.user);

    const loginSubmit = (data) => {
        dispatch(loginRequest({ email: data.email, password: data.password }))
    }

    const _authValid = () => {
        dispatch(authValid());
    }

    useEffect(() => {
        _authValid();
    },[])

    if (loggedIn) {
        return <Redirect to={'/'}/>;
    }

    return (
        <section className="bg-light p-5">
            <div className="container col-md-6 offset-md-3">
                <h1>Iniciar sesion</h1>
                <p className="lead"></p>

                <div className="row">
                    <div className="">
                        <form onSubmit={ handleSubmit(loginSubmit) }>
                            <div className="mb-3">
                                <label className="form-label">Correo Electrónico</label>
                                <input type="email" className="form-control" {...register("email", { required: true })} />
                                {errors.email && <span className="text-danger">Este campo es requerido!</span>}
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Contraseña</label>
                                <input type="password" className="form-control" {...register("password", { required: true })} />
                                {errors.password && <span className="text-danger">Este campo es requerido!</span>}
                            </div>
                            <button type="submit" className="btn btn-primary">Iniciar</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Login;
