import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { registerRequest, authValid } from '../store/user';
import { useForm } from 'react-hook-form';
import { Redirect, useHistory } from 'react-router-dom';

const Register = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { loggedIn } = useSelector((state) => state.user);
    const { registration } = useSelector((state) => state.user);
    const history = useHistory();

    const _registerRequest = (data, e) => {
        console.log(data);
        dispatch(registerRequest({ name: data.name, email: data.email, password: data.password, password_confirmation: data.password_confirmation, document_ID: data.document_ID, mobile_phone:data.mobile_phone }));
        !!registration && e.target.reset();
    }

    const _authValid = () => {
        dispatch(authValid());
    }

    useEffect(() => {
        _authValid();
    },[])
    

    if (loggedIn) {
        return <Redirect to={'/'}/>;
    }
    return (
        <section className="bg-light p-5">
            <div className="container col-md-6 offset-md-3">
                <h1>Regístrate</h1>
                <p className="lead"></p>

                <form onSubmit={handleSubmit(_registerRequest)} className="row g-3 d-flex justify-content-center mt-5">
                    <div className="col-md-6">
                        <label className="form-label">Nombres</label>
                        <input type="text" className="form-control" {...register("name", { required: true })}/>
                        {errors.name && <span className="text-danger">Este campo es requerido!</span>}
                    </div>
                    <div className="col-md-6">
                        <label className="form-label">Correo Electrónico</label>
                        <input type="email" className="form-control" {...register("email", { required: true })}/>
                        {errors.email && <span className="text-danger">Este campo es requerido!</span>}
                    </div>
                    <div className="col-md-6">
                        <label className="form-label">Contraseña</label>
                        <input type="password" className="form-control" {...register("password", { required: true })}/>
                        {errors.password && <span className="text-danger">Este campo es requerido!</span>}
                    </div>
                    <div className="col-md-6">
                        <label className="form-label">Confirmar Contraseña</label>
                        <input type="password" className="form-control" name="password_confirmation" {...register("password_confirmation", { required: true })}/>
                        {errors.password_confirmation && <span className="text-danger">Este campo es requerido!</span>}
                    </div>
                    <div className="col-md-6">
                        <label className="form-label">Documento ID.</label>
                        <input type="text" className="form-control" {...register("document_ID", { required: true })}/>
                        {errors.document_ID && <span className="text-danger">Este campo es requerido!</span>}
                    </div>
                    <div className="col-md-6">
                        <label className="form-label">Número telefónico</label>
                        <input type="text" className="form-control" {...register("mobile_phone", { required: true })}/>
                        {errors.mobile_phone && <span className="text-danger">Este campo es requerido!</span>}
                    </div>
                    <div className="col-12 d-flex justify-content-center p-5">
                        <button type="submit" className="btn btn-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </section>
    )
}

export default Register;
