import React from 'react';
import { useForm } from 'react-hook-form';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Fade from '@material-ui/core/Fade';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';

const ModalForm = ({loading, open, handleClose, res, handlePayment}) => {
    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    const useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
            maxWidth: 360,
            backgroundColor: theme.palette.background.paper,
        },
        section1: {
            margin: theme.spacing(3, 2),
        },
    }));
    
    const classes = useStyles();

    return (
        <Dialog
            open={open}>
            <form onSubmit={ handleSubmit(handlePayment) } className={classes.root}>
                <div className={classes.section1}>
                    <Grid container alignItems="center">
                    <Grid item xs>
                        <Typography gutterBottom variant="h4">
                        Enviar Pago
                        </Typography>
                    </Grid>
                    </Grid>
                    <Typography color="textSecondary">
                        Ingrese el monto:
                    </Typography>
                </div>
                <Divider variant="middle" />
                <DialogContent>
                    <input defaultValue={""} type="text" className="form-control" placeholder="Monto" {...register("amount", { required: true })}/>
                </DialogContent>
                <Box display="flex" justifyContent="center" m={1} p={1} bgcolor="background.paper">
                    <Fade
                        in={loading}
                        unmountOnExit>
                        <CircularProgress />
                    </Fade>
                </Box>
                <DialogActions>
                    <Button type="reset" onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button type="submit" variant="outlined" color="primary">
                        Pagar
                    </Button>
                </DialogActions>	
            </form>
        </Dialog>
    )

}


export default ModalForm;