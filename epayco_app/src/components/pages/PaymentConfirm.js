import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Fade from '@material-ui/core/Fade';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import { authValid, loadDataUser, confirmPaymentRequest, changeLoadingRequest } from '../store/user';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  	root: {
    	width: '100%',
    	maxWidth: 360,
    	backgroundColor: theme.palette.background.paper,
  	},
  	section1: {
    	margin: theme.spacing(3, 2),
  	},
}));

const PaymentConfirm = () => {
	const dispatch = useDispatch();
	const classes = useStyles();
	const { register, handleSubmit, formState: { errors }, reset } = useForm();
	const [open, setOpen] = useState(true);
	const { user } = useSelector((state) => state.user);
	const { loading } = useSelector((state) => state.user);
	const { confirmedPayment } = useSelector((state) => state.user);

	const confirmPaymentSubmit = (data, e) => {
		const url = window.location.pathname
		const id_session = url.split('/')[3];
		dispatch(changeLoadingRequest(true));
        dispatch(confirmPaymentRequest({ token: data.token, id_session: id_session, user: user && user.data }));
		if(confirmedPayment === false){
            e.target.reset();
        }
    }

	const _authValid = () => {
        dispatch(authValid());
    }

	const _loadDataUser = () => {
        dispatch(loadDataUser());
    }

	useEffect(() => {
        _authValid();
        _loadDataUser();
    }, []);

	return (
		<div>
			<Dialog
				open={open}>
				<form onSubmit={ handleSubmit(confirmPaymentSubmit) } className={classes.root}>
					<div className={classes.section1}>
						<Grid container alignItems="center">
						<Grid item xs>
							<Typography gutterBottom variant="h4">
							Confirmar Pago
							</Typography>
						</Grid>
						</Grid>
						<Typography color="textSecondary">
							Ingrese el token de confirmación:
						</Typography>
					</div>
					<Divider variant="middle" />
					<DialogContent>
						<input defaultValue={""} type="text" className="form-control" placeholder="Token" {...register("token", { required: true })}/>
					</DialogContent>
					<Box display="flex" justifyContent="center" m={1} p={1} bgcolor="background.paper">
						<Fade
							in={loading}
							unmountOnExit>
							<CircularProgress />
						</Fade>
					</Box>
					<DialogActions>
						<Button color="primary">
							Cancelar
						</Button>
						<Button type="submit" variant="outlined" color="primary">
							Confirmar
						</Button>
					</DialogActions>	
				</form>
			</Dialog>
		</div>
	);
}

export default PaymentConfirm;
