import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { authValid, loadDataUser, addFundsRequest, sendPaymentRequest, changeLoadingRequest } from '../store/user';
import { useForm } from 'react-hook-form';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ModalForm from './ModalForm';

const Wallet = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { user } = useSelector((state) => state.user);
    const { loggedIn } = useSelector((state) => state.user);
    const { loading } = useSelector((state) => state.user);
    const { sendPayment } = useSelector((state) => state.user);
    const [open, setOpen] = useState(false);
    
    const addFundsSubmit = (data, e) => {
        dispatch(addFundsRequest({operation: 'DEPOSIT', amount: data.amount, wallet_id: user && user.data.wallet.id, document_ID: data.document_ID, mobile_phone: data.mobile_phone }))
        // e.target.reset();
    }

    const sendPaymentSubmit = (data, e) => {
        dispatch(changeLoadingRequest(true));
        dispatch(sendPaymentRequest({ amount: data.amount, user_id: user && user.data.id }));
        if(sendPayment){
            e.target.reset();
        }
    }

    const handleClosePayment = () => {
        setOpen(false);
    };

    const _authValid = () => {
        dispatch(authValid());
    }
    const _loadDataUser = () => {
        dispatch(loadDataUser());
    }
    
    const getFormatedDate = (date) => {
        var new_date = new Date(date)
        
        var d = new_date.getDate();
        var m = new_date.getMonth() + 1;
        var y = new_date.getFullYear();

        if(m < 10){
            return (`${d}-0${m}-${y}`)
        }else {
            return(`${d}-${m}-${y}`)
        }
    }

    const handleClickOpen = () => {
        setOpen(true);
    };
    
    useEffect(() => {
        _authValid();
        _loadDataUser();
    }, []);

    useEffect(() => {
        if(sendPayment){
            setOpen(false);
        }
    }, [sendPayment]);

    return (
        <section className="">
                {loggedIn ?
                    (
                        <Fragment>
                            <div className="container-fluid bg-light">
                                <div className="row border-bottom pb-3">
                                    <div className="col-md-12 mt-5">
                                        <h1 className="text-center display-1" style={{fontSize: "8rem"}}>$ {user ? user.data.wallet.money : null}</h1>
                                    </div>
                
                                    <form key={1} onSubmit={ handleSubmit(addFundsSubmit) } className="row row-cols-lg-auto g-3 align-items-center justify-content-md-center">
                                        <div className="col-12">
                                            {errors.amount && <span className="text-danger">Este campo es requerido!</span>}
                                            <label className="visually-hidden">Monto</label>
                                            <div className="input-group">
                                            <div className="input-group-text">$</div>
                                            <input type="text" className="form-control" placeholder="Monto" {...register("amount", { required: true, valueAsNumber: true, })}/>
                                            </div>
                                        </div>
                                        
                                        <div className="col-12">
                                            {errors.document_ID && <span className="text-danger">Este campo es requerido!</span>}
                                            <label className="visually-hidden">Documento ID.</label>
                                            <div className="input-group">
                                            <input type="text" className="form-control" placeholder="Documento ID." {...register("document_ID", { required: true })}/>
                                            </div>
                                        </div>
                                        
                                        <div className="col-12">
                                            {errors.mobile_phone && <span className="text-danger">Este campo es requerido!</span>}
                                            <label className="visually-hidden">Teléfono</label>
                                            <div className="input-group">
                                            <input type="text" className="form-control" placeholder="Teléfono" {...register("mobile_phone", { required: true })}/>
                                            </div>
                                        </div>
      
                                        <ButtonGroup color="primary" aria-label="outlined primary button group">
                                            <Button type="submit" variant="contained">Agregar fondos</Button>
                                            <Button onClick={handleClickOpen}>Pagar</Button>
                                        </ButtonGroup>
                                    </form>
                                    <div>
                                        <ModalForm loading={loading} open={open} handleClose={handleClosePayment} handlePayment={sendPaymentSubmit}/>
                                    </div>
                                </div>
                            </div>
                            <div className="container">
                                <table className="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">ID TRANSACCION</th>
                                        <th scope="col">FECHA</th>
                                        <th scope="col">OPERACION</th>
                                        <th scope="col">MONTO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {user && user.success && user.data.wallet.transactions.map(e => 
                                            <tr key={e.id}>
                                                <th scope="row">#{e.operation_token}</th>
                                                <td>{getFormatedDate(e.created_at)}</td>
                                                <td className={`text-${e.operation === 'DEPOSIT'? 'success' : 'danger'}`}>{e.operation}</td>
                                                <td className={`text-${e.operation === 'DEPOSIT'? 'success' : 'danger'}`}>{`${e.operation === 'DEPOSIT' ? `+${e.amount}`: `-${e.amount}`}`}</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </Fragment>
                    
                    ) :
                    (
                        <figure className="text-center pt-5 pb-5">
                            <blockquote className="blockquote">
                                <h1>Bienvenido</h1>
                            </blockquote>
                            <figcaption className="blockquote-footer">
                                ePayco - Billetera virtual
                            </figcaption>
                        </figure>
                    )
                }

        </section>
    )
}

export default Wallet;
