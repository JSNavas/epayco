import { createSlice } from  "@reduxjs/toolkit";
import { api } from "../config/api";
import { toast } from 'react-toastify';

const initialState = {
    user: null,
    token: null,
    loggedIn: false,
    registration: null,
    loading: null,
    sendPayment: null,
    confirmedPayment: null,
}


const userSlice = createSlice({
    name: 'user',
    initialState: initialState,
    reducers: {
        loginSuccess: (state, action) => {
            localStorage.setItem('token', action.payload);
            state.token = action.payload;
            state.loggedIn = true;
        },
        loadDataUserSuccess: (state, action) => {
            state.user = action.payload;
        },
        registerSuccess: (state, action) => {
            state.registration = action.payload;
        },
        logoutSuccess: (state, action) => {
            state.user = null;
            state.token = null;
            state.loggedIn = false;
            localStorage.removeItem('token');
        },
        changeLoading: (state, action) => {
            state.loading = action.payload;
        },
        sendPaymentSuccess: (state, action) => {
            state.sendPayment = action.payload;
        },
        confirmPaymentSuccess: (state, action) => {
            state.confirmedPayment = action.payload;
        },
    }
})

export default userSlice.reducer;

const { loginSuccess, loadDataUserSuccess,  registerSuccess, logoutSuccess, changeLoading, sendPaymentSuccess, confirmPaymentSuccess } = userSlice.actions;

export const authValid = () => async(dispatch) => {
    if(!!localStorage.getItem('token')){
        dispatch(loginSuccess(localStorage.getItem('token')));
    }
}

export const loginRequest = ({email, password}) => async(dispatch) => {
    try {
        const response = await api.post('/api/v1/user/login', { email, password })
        dispatch(loginSuccess(response.data.token));
        toast.success(`Bienvenido!!`, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    } catch (e) {
        toast.error(e.response.data.message, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}

export const loadDataUser = () => async(dispatch) => {
    if(!!localStorage.getItem('token')){
        try {
            const response = await api.get('/api/v1/user/')
            dispatch(loadDataUserSuccess(response.data));
        } catch (e){
            toast.error('Session expirada!!', {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            dispatch(logoutSuccess());
        }
    }
}

export const registerRequest = ({name, email, password, password_confirmation, document_ID, mobile_phone}) => async(dispatch) => {
    try {
        const response = await api.post('/api/v1/user/register', { name, email, password, password_confirmation, document_ID, mobile_phone })
        dispatch(registerSuccess(true));
        toast.success(response.data.message, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        setTimeout(() => {
            window.location = "/login";
        }, 3000);
    } catch (e) {
        toast.error(e.response.data.message, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}

export const logoutRequest = () => async(dispatch) => {
    dispatch(logoutSuccess());
}

export const addFundsRequest = ({ operation, amount, wallet_id, document_ID, mobile_phone }) => async(dispatch) => {
    try {
        await api.post('/api/v1/user/transactions', { operation, amount, wallet_id, document_ID, mobile_phone })
        try {
            const response = await api.get('/api/v1/user/')
            dispatch(loadDataUserSuccess(response.data));
            toast.success('Transacción realizada exitosamente!!', {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } catch (e){
            toast.error(e.response.data.message, {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    } catch (e) {
        toast.error(e.response.data.message, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}

export const changeLoadingRequest = ( loading ) => async(dispatch) => {
    dispatch(changeLoading(loading));
}

export const sendPaymentRequest = ({ amount, user_id }) => async(dispatch) => {
    try {
        const response = await api.post('/api/v1/user/payments', { amount, user_id })
        console.log(response.data)
        
        if (response.data.success){
            dispatch(changeLoading(false));
            dispatch(sendPaymentSuccess(true));
            toast.success(response.data.message, {
                position: "top-right",
                autoClose: 6000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    } catch (e) {
        dispatch(changeLoading(false));
        dispatch(sendPaymentSuccess(false));
        toast.error(e.response.data.message, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}

export const confirmPaymentRequest = ({ token, id_session, user }) => async(dispatch) => {
    try {
        const response = await api.post(`/api/v1/user/payments/confirm?id_session=${id_session}`, { token })
        const operation = 'DEBIT';
        const amount = response.data.data.amount;
        const user_id = response.data.data.user_id;
        await api.post('/api/v1/user/transactions', { operation, amount, user_id })

        dispatch(changeLoading(false));
        dispatch(confirmPaymentSuccess(true));
        toast.success(response.data.message, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        setTimeout(() => {
            window.location = "/";
        }, 3000);
    } catch (e) {
        dispatch(changeLoading(false));
        dispatch(confirmPaymentSuccess(false));
        toast.error(e.response.data.message, {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}

